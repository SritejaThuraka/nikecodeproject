//
//  NikeCodeTestAppTests.swift
//  NikeCodeTestAppTests
//
//  Created by Sriteja Thuraka on 9/4/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import XCTest
@testable import NikeCodeTestApp

class NikeCodeTestAppTests: XCTestCase {

    func testSongsWebService() {
        let expectation = XCTestExpectation(description: "Download json details")
        WebServiceManager.fetchSongs { (songs, error) in
            XCTAssertNotNil(songs, "Failed to fetch songs")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSongsCount() {
        let expectation = XCTestExpectation(description: "Check songs count")
        WebServiceManager.fetchSongs { (songs, error) in
            if let songsReceived = songs {
                XCTAssertTrue(songsReceived.count > 0, "Received empty songs")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testImageCacheForImage() {
        let expectation = XCTestExpectation(description: "Check image")
        ImageCache.cacheImage.downloadImage(url: "https://is2-ssl.mzstatic.com/image/thumb/Music113/v4/15/ed/f3/15edf350-0804-ccd6-afa7-283713a81620/19UMGIM62721.rgb.jpg/200x200bb.png") { (image, error) in
            XCTAssertNotNil(image, "Image received is invalid")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testImageCacheForInvalidImage() {
        let expectation = XCTestExpectation(description: "Check invalid image")
        ImageCache.cacheImage.downloadImage(url: "https://www.google.com") { (image, error) in
            XCTAssertNil(image, "Something wrong")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }

}
