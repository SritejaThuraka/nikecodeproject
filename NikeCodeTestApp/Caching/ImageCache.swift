//
//  ImageCache.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/6/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

class ImageCache {
    static let cacheImage = ImageCache()
    private init() {}
    let imageCache = NSCache<NSString, UIImage>()
    
    func downloadImage(url: String, completionHandler: @escaping (_ image: UIImage?, _ error: Error?) -> Void){
        if let cacheImage =  imageCache.object(forKey: url as NSString) {
            completionHandler(cacheImage, nil)
        } else {
            WebServiceManager.fetchImage(urlToFetchImage: url) { (receivedData, receivedError) in
                if let error = receivedError {
                    completionHandler(nil, error)
                } else if let data = receivedData {
                    if let image = UIImage(data: data) {
                            completionHandler(image, nil)
                    }  else {
                        completionHandler(nil, Utility.createError(message: "Failed to fetch Image", code: 1002))
                    }
                }
            }
        }
    }
}
            

