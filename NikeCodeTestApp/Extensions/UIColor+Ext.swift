//
//  UIColor+Ext.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/9/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let redValue = CGFloat(red) / 255.0
        let greenValue = CGFloat(green) / 255.0
        let blueValue = CGFloat(blue) / 255.0
        self.init(red:redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
}
