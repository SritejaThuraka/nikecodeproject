//
//  UIViewControllerExtension.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/6/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String )  {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertView.addAction(alertAction)
        
        present(alertView, animated: true, completion: nil)
    }
}
