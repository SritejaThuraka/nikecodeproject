//
//  SongTableViewCell.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 2/15/20.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {
  
    static let cellID = "songTableViewCell"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
