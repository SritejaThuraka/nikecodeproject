//
//  WebServiceManager.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/6/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import Foundation

class WebServiceManager {
    
    static func fetchSongs(completionHandler: @escaping (_ songs: [Song]?, _ error: Error?) -> Void) {
        NetworkManager.manager.fetchData(from: Constants.songsUrl) { (receivedData, error) in
            if let error = error {
                completionHandler(nil, error)
            } else if let receivedData = receivedData {
                do {
                   let decoder = JSONDecoder()
                    let result = try decoder.decode(Object.self, from: receivedData)
                    print(result)
                    completionHandler(result.feed.results, nil)
                }
                catch {
                    print(error)
                    completionHandler(nil, error)
                }
            }
        }
        
    }
    
    static func fetchImage(urlToFetchImage: String, completionHandler: @escaping(_ image: Data?, _ error: Error?) -> Void) {
        NetworkManager.manager.fetchData(from: urlToFetchImage) { (receivedData, receivedError) in
            if let error = receivedError {
                completionHandler(nil, error)
            } else if let data = receivedData {
                completionHandler(data, nil)
            } else {
                completionHandler(nil, Utility.createError(message: "Failed to download Image", code: 1002))            }
        }
    }

}
