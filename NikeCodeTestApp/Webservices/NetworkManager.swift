//
//  NetworkManager.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/5/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import Foundation

class NetworkManager {
    static let manager = NetworkManager()
    private init() {}
    
    let urlSession = URLSession(configuration: .default)
    
    func fetchData(from url: String, completionHandler: @escaping (_ data: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: url) else {
            completionHandler(nil, Utility.createError(message: "Improper URL", code: 1001) )
            return
        }
        
        let task = urlSession.dataTask(with: url){receivedData, receivedResponse, error in
            
            if let error = error {
                completionHandler(nil, error)
            } else if let data = receivedData {
                completionHandler(data, nil)
            } else {
                completionHandler(nil, Utility.createError(message: "Failed to download Image", code: 1002))
            }
        }
        task.resume()
    }
}
