//
//  Song.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/5/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import Foundation

struct Song: Codable {
    let artistName: String
    let id: String
    let name: String
    let url: String
    let genres: [Genre]
    let artworkUrl100: String?
    let releaseDate: String
    let copyright: String
}
