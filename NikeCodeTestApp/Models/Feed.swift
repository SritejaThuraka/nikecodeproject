//
//  Feed.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/5/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import Foundation

struct Feed: Codable {
    let results : [Song]
}
