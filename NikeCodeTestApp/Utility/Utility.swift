//
//  Utility.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/5/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import Foundation

class Utility  {
    static func createError(message: String, code: Int) -> Error {
        return NSError(domain: "sriteja.NikeCodeTestApp", code: code, userInfo: nil) as Error
    }
}
