//
//  SongDetailsViewController.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/6/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

class SongDetailsViewController: UIViewController {
    var song: Song?
    
    let artImage: UIImageView = {
        let imgView = UIImageView(frame: .zero)
        imgView.image = UIImage(named: "placeholder")
        return imgView
    }()
    let imageView: UIImageView = {
        let imgView = UIImageView(frame: .zero)
        imgView.image = UIImage(named: "placeholder")
        imgView.contentMode = .scaleToFill
        return imgView
    }()
    
    let artistName: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = UIFont(name: "Helvetica", size: 16)
        lbl.textColor = UIColor(red: 178, green: 177, blue: 178)
        return lbl
    }()
    
    let genre: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = UIFont(name: "Helvetica", size: 16)
        lbl.textColor = UIColor(red: 178, green: 177, blue: 178)
        return lbl
    }()
    
    let releaseDate: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = UIFont(name: "Helvetica", size: 16)
        lbl.textColor = UIColor(red: 178, green: 177, blue: 178)
        return lbl
    }()
    
    let copyRight: UILabel = {
        let lbl = UILabel(frame: .zero)
        lbl.lineBreakMode = .byWordWrapping
        lbl.numberOfLines = 0
        lbl.font = UIFont(name: "Helvetica", size: 16)
        lbl.textColor =  UIColor(red: 178, green: 177, blue: 178)
        return lbl
    }()
    
    let appstoreButton: UIButton = {
        let btn = UIButton(type: .roundedRect)
        btn.setTitle("Open with iTunes", for: .normal)
        btn.backgroundColor =  UIColor(red: 234,green: 66, blue: 88)
        btn.layer.cornerRadius = 20
        btn.layer.shadowOpacity = 0.5
        btn.layer.masksToBounds =  true
        btn.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        btn.setTitleColor(.white, for: .normal)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = UIColor(red: 234,green: 66, blue: 88)
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor(red: 234,green: 66, blue: 88)]

        setupData()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.barTintColor = .white
    }
    
    
    
    func setupView() {
        imageView.frame = view.bounds
        view.addSubview(imageView)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = imageView.bounds
        view.addSubview(blurredEffectView)
        
        let stackView = UIStackView(arrangedSubviews: [artistName, genre, releaseDate, copyRight])
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.alignment = .fill
        stackView.distribution = .fill
        view.addSubview(stackView)
        view.addSubview(artImage)
        view.addSubview(appstoreButton)
        appstoreButton.addTarget(self, action: #selector(openInStore(_:)), for: .touchUpInside)
        
        
        artImage.translatesAutoresizingMaskIntoConstraints = false
        artImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 100).isActive = true
        artImage.widthAnchor.constraint(equalToConstant: 200).isActive = true
        artImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        artImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
      
        appstoreButton.translatesAutoresizingMaskIntoConstraints = false
        appstoreButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        appstoreButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        appstoreButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive = true
        appstoreButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        stackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        stackView.topAnchor.constraint(equalTo: artImage.bottomAnchor, constant: 20).isActive = true
        
       
    }
    

    @objc func openInStore(_ btn: UIButton) {
        if let songToOpen = song, let url = URL(string: songToOpen.url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func setupData() {
        if let songToOpen = song {
            title = songToOpen.name
            if let imgURL = songToOpen.artworkUrl100 {
                ImageCache.cacheImage.downloadImage(url: imgURL, completionHandler: { (img, error) in
                    if let receivedImage = img {
                        DispatchQueue.main.async {
                            self.artImage.image = receivedImage
                            self.imageView.image = receivedImage
                        }
                    }
                })
            }
            artistName.text = "Artist: " + songToOpen.artistName
            genre.text = "Genre: " + songToOpen.genres.map{ $0.name }.joined(separator: ", ")
            releaseDate.text = "Released Date: " + songToOpen.releaseDate
            copyRight.text = "Copyright: " + songToOpen.copyright
        }
    }
}
