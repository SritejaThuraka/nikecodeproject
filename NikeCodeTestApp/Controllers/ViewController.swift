//
//  ViewController.swift
//  NikeCodeTestApp
//
//  Created by Sriteja Thuraka on 9/4/19.
//  Copyright © 2019 Sriteja Thuraka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var songs = [Song]()
    let songsList: UITableView = {
        let tableView = UITableView(frame: .zero)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Songs"
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(loadSongs))
        navigationItem.rightBarButtonItem = refreshButton
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red: 234,green: 66, blue: 88)
        
        setUpTableView()
        setUpRegisterForTableView()
        loadSongs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = UIColor(red: 234,green: 66, blue: 88)
    }
    
    func setUpRegisterForTableView() {
        songsList.register(SongTableViewCell.self, forCellReuseIdentifier: SongTableViewCell.cellID)
        songsList.dataSource = self
        songsList.delegate = self
        songsList.tableFooterView = UIView()
    }
    
    //MARK: - This function setups the tableview
    func setUpTableView() {
        view.addSubview(songsList)
        songsList.translatesAutoresizingMaskIntoConstraints =  false
        songsList.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        songsList.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        songsList.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        songsList.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    }
    
    
    //MARK: - This function loads the songs list from url
    @objc func loadSongs() {
        WebServiceManager.fetchSongs { (songs, error) in
            DispatchQueue.main.async {
                if let err = error {
                    print(err)
                    self.showAlert(title: "OOPS", message: "Failed to fetch songs. Please check internet and try again")
                }
                if let songsReceived = songs {
                   self.songs = songsReceived
                   self.songsList.reloadData()
                }
            }
        }

    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         print("kakaka\(songs.count)")
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SongTableViewCell.cellID, for: indexPath) as? SongTableViewCell else {
            return UITableViewCell()
        }
        let song = songs[indexPath.row]
        cell.textLabel?.text = song.name
        cell.detailTextLabel?.text = song.artistName
        cell.detailTextLabel?.textColor = UIColor.gray
        cell.imageView?.image = UIImage(named: "placeholder")
        
        if let imageUrl = song.artworkUrl100 {
                ImageCache.cacheImage.downloadImage(url: imageUrl) { (receivedImage, receivedError) in
                    DispatchQueue.main.async {
                        if let img = receivedImage {
                            cell.imageView?.image = img
                        }
                    }
                }
            }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let songAtIndex = songs[indexPath.row]
        let detailView = SongDetailsViewController(nibName: nil, bundle: nil)
        detailView.song = songAtIndex
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

